**DESAFIO - DESENVOLVIMENTO MOBILE - FLUTTER**

Objetos:

1. Avaliar a capacidade analítica do candidato
2. Avaliar o conhecimento do usuário ao implementar a solução



---

## Desafio

Implemente o aplicativo baseado no protótipo a seguir: https://www.figma.com/file/JQ7AGsbKVW23olmGDwKl4e/Teste-04%2F2021?node-id=0%3A1.

Essa aplicação será utilizada para realizar o agendamento de visitas técnicas.
O usuário deverá conseguir se logar na ferramenta, visualizar profissionais e agendar uma 
visita. Não é necessário desenvolver uma tela de cadastro de usuário ou tela de cadastro dos 
profissionais.

---

## Será avaliado a criação e funcionalidade das telas:

**Tela de Login**.

1. Usuário
2. Senha

**Tela Principal**.

1. Lista de profissionais com nome de cada um


**Agendamento ao clicar em um profissional**.

1. Data do agendamento
2. Hora do agendamento
3. Endereço de atendimento
4. Tipo de serviço

**Realização do Pull Request**

1. Realize um Fork desse repositório
2. Após finalizar, realize um pull request para branch desafio_flutter 


## Boa sorte!!!!



